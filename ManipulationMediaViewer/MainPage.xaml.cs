﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ManipulationMediaViewer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        Point centerPoint => new Point(ManipulationMediaViewer.ActualWidth / 2, ManipulationMediaViewer.ActualHeight / 2);

        void ZoomIn_OnClick(object sender, RoutedEventArgs e)
        {
            ManipulationMediaViewer.ZoomIn();
        }
        void ZoomOut_OnClick(object sender, RoutedEventArgs e)
        {
            ManipulationMediaViewer.ZoomOut();
        }
        void Reset_OnClick(object sender, RoutedEventArgs e)
        {
            ManipulationMediaViewer.Reset();
        }

        void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            //Frame.Navigate(typeof(TransformSimulator), null);
        }
    }
}
