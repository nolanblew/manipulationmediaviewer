﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Devices.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using ManipulationMediaViewer.Annotations;
using ManipulationMediaViewer.Helpers;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ManipulationMediaViewer
{
    [ContentProperty(Name = "ViewboxContent")]
    public sealed partial class ManipulationMediaViewer : UserControl, INotifyPropertyChanged
    {
        public ManipulationMediaViewer()
        {
            this.InitializeComponent();
        }

        #region Dependency Properties

        public static readonly DependencyProperty ViewboxContentProperty = DependencyProperty.Register(nameof(ViewboxContent), typeof(UIElement), typeof(ManipulationMediaViewer), null);

        /// <summary>
        /// Content of the ManipulationMediaViewer
        /// </summary>
        public UIElement ViewboxContent
        {
            get { return (UIElement)GetValue(ViewboxContentProperty); }
            set { SetValue(ViewboxContentProperty, value); }
        }

        public static readonly DependencyProperty MinZoomProperty = DependencyProperty.Register(nameof(MinZoom), typeof(double), typeof(ManipulationMediaViewer), PropertyMetadata.Create(1.0));

        /// <summary>
        /// Minimum allowed zoom. Default is 1
        /// </summary>
        public double MinZoom
        {
            get { return (double)GetValue(MinZoomProperty); }
            set { SetValue(MinZoomProperty, value); }
        }

        public static readonly DependencyProperty MaxZoomProperty = DependencyProperty.Register(nameof(MaxZoom), typeof(double), typeof(ManipulationMediaViewer), PropertyMetadata.Create(10.0));

        /// <summary>
        /// Maximum allowed zoom. Default is 10
        /// </summary>
        public double MaxZoom
        {
            get { return (double)GetValue(MaxZoomProperty); }
            set { SetValue(MaxZoomProperty, value); }
        }

        /// <summary>
        /// Gets the Zoom factor
        /// </summary>
        public double ZoomFactor => (ViewboxHost.RenderTransform as CompositeTransform).ScaleX;

        public static readonly DependencyProperty IsZoomEnabledProperty = DependencyProperty.Register(nameof(IsZoomEnabled), typeof(bool), typeof(ManipulationMediaViewer), PropertyMetadata.Create(true));

        /// <summary>
        /// Sets whether or not zoom is enabled
        /// </summary>
        public bool IsZoomEnabled
        {
            get { return (bool)GetValue(IsZoomEnabledProperty); }
            set { SetValue(IsZoomEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsMouseEnabledProperty = DependencyProperty.Register(nameof(IsMouseEnabled), typeof(bool), typeof(ManipulationMediaViewer), PropertyMetadata.Create(true));

        /// <summary>
        /// Sets whether or not mouse input is enabled
        /// </summary>
        public bool IsMouseEnabled
        {
            get { return (bool)GetValue(IsMouseEnabledProperty); }
            set { SetValue(IsMouseEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsTouchEnabledProperty = DependencyProperty.Register(nameof(IsTouchEnabled), typeof(bool), typeof(ManipulationMediaViewer), PropertyMetadata.Create(true));

        /// <summary>
        /// Sets whether or not touch input is enabled
        /// </summary>
        public bool IsTouchEnabled
        {
            get { return (bool)GetValue(IsTouchEnabledProperty); }
            set { SetValue(IsTouchEnabledProperty, value); }
        }

        #endregion

        #region Private Properties

        bool _isStartup = true;

        CompositeTransform Transform => ViewboxHost.RenderTransform as CompositeTransform;

        ManipulationModes? _currentManipulationMode;

        int _manipulationCaptureCount = 3;

        bool _cancelManipulation = false;

        #endregion

        #region Public Methods

        /// <summary>
        /// Zooms to a specified zoom level within the min/max limits with animation
        /// </summary>
        /// <param name="level">The factor to zoom to</param>
        public void ZoomToFactor(double level, Point center)
        {
            ZoomToFactor(level, center, true);
        }

        /// <summary>
        /// Zooms to a specified zoom level within the min/max limits
        /// </summary>
        /// <param name="level">The factor to zoom to</param>
        /// <param name="playAnimation">Weather or not to play an animation to get to the zoom</param>
        public void ZoomToFactor(double level, Point center, bool playAnimation)
        {
            // TODO: Fix animation so it has a smooth handoff
            if (ZoomAnimation.GetCurrentState() == ClockState.Active)
            {
                return;
            }

            level = ManipulationHelper.Between(MinZoom, MaxZoom, level);

            var adjustedCenter = ManipulationHelper.GetZoomTranslateFactor(level, center, Transform);
            var boundingBox = new Rect(0, 0, ViewboxHost.ActualWidth * level, ViewboxHost.ActualHeight * level);
            adjustedCenter.X = ManipulationHelper.Between(-boundingBox.Width + Grid.ActualWidth, 0, adjustedCenter.X);
            adjustedCenter.Y = ManipulationHelper.Between(-boundingBox.Height + Grid.ActualHeight, 0, adjustedCenter.Y);

            var centerWidth = _centerWidthAdjustment(level);
            if (centerWidth.X > 0)
            {
                adjustedCenter.X += centerWidth.X;
            }
            if (centerWidth.Y > 0)
            {
                adjustedCenter.Y += centerWidth.Y;
            }

            if (playAnimation)
            {
                ManipulationHelper.SetZoomStoryboardChildren(ref ZoomAnimation, level, adjustedCenter);

                ZoomAnimation.Begin();
            }
            else
            {
                Transform.ScaleX = Transform.ScaleY = level;
                Transform.TranslateX = adjustedCenter.X;
                Transform.TranslateY = adjustedCenter.Y;
            }
        }

        /// <summary>
        /// Zoom in on the image where the view is centered
        /// </summary>
        /// <remarks>This zooms in by a factor (percentage), not to a specific factor. Zoooming in from 4 by a factor of 1 does not result in a zoom factor at 5. Use ZoomToFactor for that. Factor of 1 = 10% of maximum zoom</remarks>
        /// <param name="factor">The factor to zoom by. Default: 1</param>
        public void ZoomIn(double factor = 1.0)
        {
            var centerPoint = new Point(Grid.ActualWidth / 2, Grid.ActualHeight / 2);
            var zoomPercent = ZoomFactor / MaxZoom * 10;
            ZoomToFactor(ZoomFactor + factor * zoomPercent, centerPoint);
        }

        /// <summary>
        /// Zoom out on the image where the view is centered
        /// </summary>
        /// <remarks>This zooms out by a factor (percentage), not to a specific factor. Zoooming out from 5 by a factor of 1 does not result in a zoom factor at 4. Use ZoomToFactor for that. Factor of 1 = 10% of maximum zoom</remarks>
        /// <param name="factor">The factor to zoom by. Default: 1</param>
        public void ZoomOut(double factor = 1.0)
        {
            var centerPoint = new Point(Grid.ActualWidth / 2, Grid.ActualHeight / 2);
            var zoomPercent = ZoomFactor / MaxZoom * 10;
            ZoomToFactor(ZoomFactor - factor * zoomPercent, centerPoint);
        }

        /// <summary>
        /// Translate the content by a certain amount
        /// </summary>
        /// <param name="deltaX">The x-amount to translate</param>
        /// <param name="deltaY">The y-amount to translate</param>
        /// <param name="isAnimation">Wether or not to play a smoothing animation to the translate</param>
        public void Translate(double deltaX, double deltaY, bool isAnimation = true)
        {
            var translatePoint = _GetTranslateInBounds(deltaX, deltaY);

            if (isAnimation)
            {
                ManipulationHelper.SetTranslateStoryboardChildren(ref ZoomAnimation, translatePoint);
                ZoomAnimation.Begin();
            }
            else
            {
                Transform.TranslateX = translatePoint.X;
                Transform.TranslateY = translatePoint.Y;
            }
        }

        /// <summary>
        /// Reset the content to their start positions
        /// </summary>
        public void Reset()
        {
            ManipulationHelper.SetZoomStoryboardChildren(ref ZoomAnimation, 1d, _centerWidthAdjustment(1));

            Transform.CenterX = 0;
            Transform.CenterY = 0;
            Transform.Rotation = 0;

            ZoomAnimation.Begin();
        }

        #endregion

        #region Private Methods

        Point _centerWidthAdjustment(double newLevel)
        {
            var boundingBox = new Rect(0, 0, ViewboxHost.ActualWidth * newLevel, ViewboxHost.ActualHeight * newLevel);
            return new Point(
                (Grid.ActualWidth / 2) - (boundingBox.Width / 2),
                (Grid.ActualHeight / 2) - (boundingBox.Height / 2));
        }

        Point _GetTranslateInBounds(double translationDeltaX, double translationDeltaY)
        {
            var contentBox = new Rect(0, 0, ViewboxHost.ActualWidth, ViewboxHost.ActualHeight);
            var gridBox = new Rect(0, 0, Grid.ActualWidth, Grid.ActualHeight);
            var boundingBox = Transform.TransformBounds(contentBox);

            boundingBox.X += translationDeltaX;
            boundingBox.Y += translationDeltaY;

            var finalTranslateX = Transform.TranslateX + translationDeltaX;
            var finalTranslateY = Transform.TranslateY + translationDeltaY;

            if (boundingBox.X > gridBox.X)
            {
                finalTranslateX = 0;
            }
            else if (boundingBox.X + boundingBox.Width < gridBox.Width)
            {
                finalTranslateX = gridBox.Width - boundingBox.Width;
            }
            if (boundingBox.Y > gridBox.Y)
            {
                finalTranslateY = 0;
            }
            else if (boundingBox.Y + boundingBox.Height < gridBox.Height)
            {
                finalTranslateY = gridBox.Height - boundingBox.Height;
            }

            if (boundingBox.Width < ActualWidth)
            {
                finalTranslateX = Transform.TranslateX;
            }
            if (boundingBox.Height < ActualHeight)
            {
                finalTranslateY = Transform.TranslateY;
            }

            return new Point(finalTranslateX, finalTranslateY);
        }

        #endregion

        #region UI control Events

        void ContentGrid_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_isStartup && !e.NewSize.IsEmpty)
            {
                Reset();
                _isStartup = false;
            }
        }

        void ViewboxHost_OnManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            if (!IsTouchEnabled && e.PointerDeviceType == PointerDeviceType.Touch ||
                !IsMouseEnabled && e.PointerDeviceType == PointerDeviceType.Mouse)
            {
                e.Complete();
                return;
            }

            _currentManipulationMode = null;
            _manipulationCaptureCount = 3;
        }

        void ViewboxHost_OnManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (!IsTouchEnabled && e.PointerDeviceType == PointerDeviceType.Touch ||
                !IsMouseEnabled && e.PointerDeviceType == PointerDeviceType.Mouse ||
                _cancelManipulation)
            {
                _cancelManipulation = false;
                e.Complete();
                return;
            }

            if (_manipulationCaptureCount > 0)
            {
                _manipulationCaptureCount--;
                return;
            }

            if (_currentManipulationMode == null)
            {
                var translateDistance =
                    Math.Sqrt(Math.Pow(e.Delta.Translation.X, 2) + Math.Pow(e.Delta.Translation.Y, 2));

                var zoom = IsZoomEnabled ? Math.Abs((e.Delta.Scale - 1) * 200) : 0;
                if (zoom > translateDistance)
                {
                    _currentManipulationMode = ManipulationModes.Zoom;
                }
                else if (translateDistance > zoom)
                {
                    _currentManipulationMode = ManipulationModes.Translate;
                }
            }

            if (_currentManipulationMode == ManipulationModes.Translate)
            {
                var translate = _GetTranslateInBounds(e.Delta.Translation.X, e.Delta.Translation.Y);

                // TODO: Stop the control from continuing with interia

                Transform.TranslateX = translate.X;
                Transform.TranslateY = translate.Y;
            }

            if (_currentManipulationMode == ManipulationModes.Zoom)
            {
                var position = Transform.TransformPoint(e.Position);

                var zoomDelta = e.Delta.Scale - 1;
                ZoomToFactor(ZoomFactor + (zoomDelta * ZoomFactor), position, false);
            }
        }

        void ViewboxHost_OnPointerWheelChanged(object sender, PointerRoutedEventArgs e)
        {
            if (!IsMouseEnabled || !IsZoomEnabled) { return; }

            var point = e.GetCurrentPoint(Grid);
            var zoomPercent = ZoomFactor / MaxZoom * 2;
            var delta = point.Properties.MouseWheelDelta / 100 * zoomPercent;

            ZoomToFactor(ZoomFactor + delta, point.Position, false);
        }

        #endregion

        [Flags]
        enum ManipulationModes
        {
            None, Zoom, Translate
        }

        #region Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
