﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ManipulationMediaViewer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TransformSimulator : Page
    {
        public TransformSimulator()
        {
            this.InitializeComponent();
        }

        #region Grid Overlay

        int _gridSize = 100;

        void OverlayGrid(bool includeCoordinates = false, Grid appliedGrid = null)
        {
            if (appliedGrid == null) appliedGrid = MainGrid;

            int rows = (int)(MainGrid.ActualWidth / _gridSize) + 1;
            int cols = (int)(MainGrid.ActualHeight / _gridSize) + 1;

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    CreateGridRect(r, c, includeCoordinates, appliedGrid);
                }
            }

            _AddPoint();
        }

        void CreateGridRect(int row, int col, bool includeCoordinates = false, Grid appliedGrid = null)
        {
            if (appliedGrid == null) appliedGrid = MainGrid;

            Rectangle rct = new Rectangle();
            rct.Stroke = new SolidColorBrush(Colors.LightSkyBlue);
            rct.Width = _gridSize;
            rct.Height = _gridSize;
            rct.HorizontalAlignment = HorizontalAlignment.Left;
            rct.VerticalAlignment = VerticalAlignment.Top;

            rct.Margin = new Thickness(row * _gridSize, col * _gridSize, 0, 0);
            rct.Visibility = Visibility.Visible;
            MainGrid.Children.Add(rct);

            if (includeCoordinates)
            {
                TextBlock textBlock = new TextBlock();
                textBlock.Text = $"({row * _gridSize}, {col * _gridSize})";
                textBlock.Foreground = new SolidColorBrush(Colors.Gray);
                textBlock.FontSize = 8;

                textBlock.HorizontalAlignment = HorizontalAlignment.Left;
                textBlock.VerticalAlignment = VerticalAlignment.Top;
                textBlock.Margin = new Thickness(row * _gridSize + 1, col * _gridSize, 0, 0);
                textBlock.Visibility = Visibility.Visible;

                MainGrid.Children.Add(textBlock);
            }
        }

        void _AddPoint()
        {
            Rectangle rct = new Rectangle();
            rct.Stroke = new SolidColorBrush(Colors.Red);
            rct.Fill = new SolidColorBrush(Colors.Red);
            rct.Width = 4;
            rct.Height = 4;
            rct.HorizontalAlignment = HorizontalAlignment.Left;
            rct.VerticalAlignment = VerticalAlignment.Top;
            rct.Margin = new Thickness(100, 100, 0, 0);
            rct.Visibility = Visibility.Visible;
            ContentGrid.Children.Add(rct);

            Rectangle rct2 = new Rectangle();
            rct2.Stroke = new SolidColorBrush(Colors.Blue);
            rct2.Fill = new SolidColorBrush(Colors.Blue);
            rct2.Width = 4;
            rct2.Height = 4;
            rct2.HorizontalAlignment = HorizontalAlignment.Left;
            rct2.VerticalAlignment = VerticalAlignment.Top;
            rct2.Margin = new Thickness(300, 200, 0, 0);
            rct2.Visibility = Visibility.Visible;
            ContentGrid.Children.Add(rct2);

            Rectangle rct3 = new Rectangle();
            rct3.Stroke = new SolidColorBrush(Colors.Green);
            rct3.Width = 100;
            rct3.Height = 100;
            rct3.HorizontalAlignment = HorizontalAlignment.Left;
            rct3.VerticalAlignment = VerticalAlignment.Top;
            rct3.Margin = new Thickness(250, 150, 0, 0);
            rct3.Visibility = Visibility.Visible;
            ContentGrid.Children.Add(rct3);
        }

        void TransformSimulator_OnLoaded(object sender, RoutedEventArgs e)
        {
            OverlayGrid(true);
        }

        #endregion

        public async void StartSim()
        {
            TimeSpan delay = TimeSpan.FromSeconds(3);

            _SetCenter(0, 0);

            _TransformGrid(50, 50);

            await Task.Delay(delay);

            _TransformGrid(new Point(250, 150));

            await Task.Delay(delay);

            _TransformGrid(new Point(250, 150));
        }

        void _SetCenter(int x, int y)
        {
            var t = TransformGrid.RenderTransform as CompositeTransform;

            t.CenterX = x;
            t.CenterY = y;
        }

        void _TransformGrid(Point center)
        {
            _TransformGrid(center.X, center.Y);
        }

        void _TransformGrid(double centerx, double centery)
        {
            var t = TransformGrid.RenderTransform as CompositeTransform;

            var oldPoint = new Point((centerx - t.TranslateX) / t.ScaleX, (centery - t.TranslateY) / t.ScaleY);

            t.ScaleY *= 2.149326f;
            t.ScaleX *= 2.149326f;

            var endPoint = new Point((oldPoint.X * t.ScaleX - centerx) * -1, (oldPoint.Y * t.ScaleY - centery) * -1);

            t.TranslateX = endPoint.X;
            t.TranslateY = endPoint.Y;
        }

        Point _transformOriginalPoint(Point point)
        {
            var t = TransformGrid.RenderTransform as CompositeTransform;
            return new Point(point.X * t.ScaleX, point.Y * t.ScaleY);
        }

        void UIElement_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            StartSim();
        }
    }
}
