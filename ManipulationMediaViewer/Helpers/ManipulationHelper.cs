﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace ManipulationMediaViewer.Helpers
{
    internal static class ManipulationHelper
    {
        /// <summary>
        /// Gets the new x-y translation offset based off of a future zoom
        /// </summary>
        /// <param name="newZoom">The new zoom level to calculate the new offset</param>
        /// <param name="position">The current centerpoint relative to the screen (ie scale 1)</param>
        /// <param name="transform">The current composite transform of the control before the zoom is applied</param>
        /// <returns></returns>
        public static Point GetZoomTranslateFactor(double newZoom, Point position, CompositeTransform transform)
        {
            var oldPoint = new Point((position.X - transform.TranslateX) / transform.ScaleX, (position.Y - transform.TranslateY) / transform.ScaleY);
            var endPoint = new Point((oldPoint.X * newZoom - position.X) * -1, (oldPoint.Y * newZoom - position.Y) * -1);
            return endPoint;
        }

        /// <summary>
        /// Sets the storyboard components to those required to enact the zoom
        /// </summary>
        /// <param name="storyboard">The zoom animation storyboard</param>
        /// <param name="zoomFactor">The level to zoom to</param>
        /// <param name="centerPoint">The center point of the zoom</param>
        public static void SetZoomStoryboardChildren(ref Storyboard storyboard, double zoomFactor, Point centerPoint)
        {
            foreach (var animation in storyboard.Children.Where(a => a is DoubleAnimation))
            {
                var doubleAnimation = animation as DoubleAnimation;
                if (doubleAnimation == null) { return; }

                var targetProperty = doubleAnimation.GetValue(Storyboard.TargetPropertyProperty).ToString();

                if (targetProperty.Contains("Scale"))
                {
                    doubleAnimation.To = zoomFactor;
                }
                else if (targetProperty.Contains("TranslateX"))
                {
                    doubleAnimation.To = centerPoint.X;
                }
                else if (targetProperty.Contains("TranslateY"))
                {
                    doubleAnimation.To = centerPoint.Y;
                }
            }
        }

        //public static Point _GetTranslateInBoundsAbsolute(double finalDeltaX, double finalDeltaY)
        //{
        //    var deltaX = finalDeltaX - Transform.TranslateX;
        //    var deltaY = finalDeltaY - Transform.TranslateY;

        //    var newTranslateDelta = _GetTranslateInBounds(deltaX, deltaY);

        //    return new Point(Transform.TranslateX + newTranslateDelta.X, Transform.TranslateY + newTranslateDelta.Y);
        //}

        //public static Point _GetTranslateInBounds(double translationDeltaX, double translationDeltaY, FrameworkElement transformToControl, FrameworkElement transformContainer)
        //{
        //    double border_region = 35;

        //    var contentBox = new Rect(0, 0, transformToControl.ActualWidth, transformToControl.ActualHeight);
        //    var gridBox = new Rect(0, 0, transformContainer.ActualWidth, transformContainer.ActualHeight);
        //    var boundingBox = Transform.TransformBounds(contentBox);
        //    boundingBox.X += translationDeltaX;
        //    boundingBox.Y += translationDeltaY;

        //    var finalTranslateX = Transform.TranslateX + translationDeltaX;
        //    var finalTranslateY = Transform.TranslateY + translationDeltaY;

        //    if (boundingBox.X > gridBox.X)
        //    {
        //        finalTranslateX = 0;
        //    }
        //    else if (boundingBox.X + boundingBox.Width < gridBox.Width)
        //    {
        //        finalTranslateX = gridBox.Width - boundingBox.Width;
        //    }
        //    if (boundingBox.Y > gridBox.Y)
        //    {
        //        finalTranslateY = 0;
        //    }
        //    else if (boundingBox.Y + boundingBox.Height < gridBox.Height)
        //    {
        //        finalTranslateY = gridBox.Height - boundingBox.Height;
        //    }

        //    if (boundingBox.Width < ActualWidth)
        //    {
        //        finalTranslateX = 0;
        //    }
        //    if (boundingBox.Height < ActualHeight)
        //    {
        //        finalTranslateY = 0;
        //    }


        //    return new Point(finalTranslateX, finalTranslateY);
        //}

        /// <summary>
        /// Sets the storyboard components to those required to enact a translate
        /// </summary>
        /// <param name="storyboard">The zoom animation storyboard</param>
        /// <param name="newPoint">The new point to translate to</param>
        public static void SetTranslateStoryboardChildren(ref Storyboard storyboard, Point newPoint)
        {
            foreach (var animation in storyboard.Children.Where(a => a is DoubleAnimation))
            {
                var doubleAnimation = animation as DoubleAnimation;
                if (doubleAnimation == null) { return; }

                var targetProperty = doubleAnimation.GetValue(Storyboard.TargetPropertyProperty).ToString();

                if (targetProperty.Contains("TranslateX"))
                {
                    doubleAnimation.To = newPoint.X;
                }
                else if (targetProperty.Contains("TranslateY"))
                {
                    doubleAnimation.To = newPoint.Y;
                }
            }
        }

        public static double Between(double min, double max, double number)
        {
            return Math.Min(Math.Max(number, min), max);
        }

    }
}
